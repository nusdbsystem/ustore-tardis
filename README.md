## Workflow  ##

The [Forking Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow) is adopted in the development. 
See the figure below for an intuitive understanding.

![Figure: Git Forking Workflow](http://www.comp.nus.edu.sg/~linqian/share/git_forking_workflow.png)

Every developer should fork the [main repository](https://bitbucket.org/nusdbsystem/ustore-tardis) to create a local copy, and work on the local copy. 
When a feature is ready to be merged into the main repository, the development should firstly *rebase his/her local repository* to the latest main repository and then *issue a pull request*. 
By rebasing the local repository, it is the developer's responsibility to resolve the conflicts if any. 
Once the repository administrator receives the pull request, he/she will process it to merge the code. 

**NOTE:** 
For every commit, the developer *must* provide a change log.
Commit without log is highly discouraged. 
