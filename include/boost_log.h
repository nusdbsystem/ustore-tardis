#ifndef _BOOST_LOG_H
#define _BOOST_LOG_H

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/console.hpp>

void init_boost_log(const boost::log::trivial::severity_level &);

#endif /* _BOOST_LOG_H */
