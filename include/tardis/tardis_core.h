#ifndef TARDIS_CORE_H
#define TARDIS_CORE_H

#include <boost/enable_shared_from_this.hpp>
#include <boost/functional/hash.hpp>
#include <boost/optional.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include <iostream>
#include <queue>
#include <utility>
#include <vector>
#include "tardis/tardis_data.h"

namespace ustore {
namespace tardis {

class Store;
class ForkPoint;
class State;
class Txn;
class StateDag;

typedef boost::mutex Mutex;
typedef boost::unique_lock<Mutex> Lock;
typedef boost::lock_guard<Mutex> LockGuard;
typedef boost::shared_mutex SharedMutex;
typedef boost::shared_ptr<SharedMutex> SharedMutexPtr;
typedef boost::shared_lock<SharedMutex> ReadLock;
typedef boost::unique_lock<SharedMutex> WriteLock;
typedef boost::lock_guard<SharedMutex> WriteLockGuard;

typedef boost::shared_ptr<ForkPoint> ForkPointPtr;
typedef boost::unordered_set<ForkPointPtr> ForkPointPtrSet;
typedef boost::shared_ptr<State> StatePtr;
typedef boost::optional<StatePtr> StatePtrOpt;
typedef std::vector<StatePtr> StatePtrVec;
typedef std::queue<StatePtr> StatePtrQue;
typedef boost::shared_ptr<StatePtrVec> StatePtrVecPtr;
typedef boost::unordered_set<StatePtr> StatePtrSet;
typedef boost::unordered_set<ClientId> ClientIdSet;
typedef boost::shared_ptr<StateDag> StateDagPtr;

typedef boost::shared_ptr<Store> StorePtr;
typedef std::vector<StoreKey> StoreKeyVec;
typedef boost::unordered_set<StoreKey> StoreKeySet;
typedef boost::shared_ptr<StoreKeySet> StoreKeySetPtr;
typedef boost::unordered_map<StoreKey, StatePtrVecPtr> StoreKey2StatePtrVecPtr;
typedef boost::unordered_map<StoreKey, SharedMutexPtr> StoreKey2SharedMutexPtr;
typedef boost::optional<StoreVal> StoreValOpt;
typedef std::pair<StoreKey, StateId> PairStoreKeyStateId;
typedef boost::unordered_map<PairStoreKeyStateId, StoreVer,
        boost::hash<PairStoreKeyStateId> > StoreKeyStateId2StoreVer;

/* Adaptor to the multi-version key-value store */
class Store {
  public:
    static StorePtr instance();

    ~Store();
    StoreVal get(const StoreKey &, const StatePtr &) const;
    void put(const StoreKey &, const StatePtr &, const StoreVal &);
    void erase(const StoreKey &, const StatePtr &);
    inline StatePtrVecPtr get_versions(const StoreKey &) const;
    inline SharedMutexPtr get_mutex_for_state_vec(const StoreKey &) const;

  private:
    static const StoreVer INIT_STORE_VER;

    StoreKeyStateId2StoreVer ki2v; // (key, state_id) -> store_ver
    StoreKey2StatePtrVecPtr k2i; // key -> vector(state)
    StoreKey2SharedMutexPtr k2mtx_state_vec;

    SharedMutexPtr mtx_ki2v;
    SharedMutexPtr mtx_k2i;
    SharedMutexPtr mtx_k2mtx_state_vec;

    Store();
    inline StoreVal get_impl(const StoreKey &, const StoreVer &) const;
    inline StoreVer put_impl(const StoreKey &, const StoreVer &, const StoreVal &) const;
    inline void erase_impl(const StoreKey &, const StoreVer &) const;
    inline StoreVer get_store_ver(const StoreKey &, const StatePtr &) const;
    inline void add_key_version_mapping(const StoreKey &, const StatePtr &);
    inline void delete_key_version_mapping(const StoreKey &, const StatePtr &);
    inline void add_store_ver_mapping(const StoreKey &, const StatePtr &, const StoreVer &);
    inline void delete_store_ver_mapping(const StoreKey &, const StatePtr &);
};

class ForkPoint {
  public:
    const StatePtr state;
    const int branch_id;

    ForkPoint(const StatePtr &, const int &);
    ~ForkPoint();
    inline bool operator==(const ForkPointPtr &) const;
    inline bool operator<(const ForkPointPtr &) const;

    friend std::ostream & operator<<(std::ostream &, const ForkPoint &);

  private:
};

class State : public boost::enable_shared_from_this<State> {
  public:
    const StateId id;
    const ClientId client_id;
    bool is_valid;

    SharedMutexPtr mtx_children;
    SharedMutexPtr mtx_fork_path;
    SharedMutexPtr mtx_pin_cnt;

    State(const StateId &, const ClientId &);
    ~State();
    inline bool operator==(const StatePtr &) const;
    inline bool operator<(const StatePtr &) const;
    inline bool has_ancestor_of(const ClientId &) const;
    inline const StatePtrSet & get_parents() const;
    inline const StatePtrVec & get_children() const;
    inline void add_child(const StatePtr &);
    inline void add_fork_point(const ForkPointPtr &);
    inline const ForkPointPtrSet & get_fork_path() const;
    inline void commit_after(const StatePtr &);
    inline bool is_compatible_with(const StatePtr &) const;
    inline bool is_leaf() const;
    inline bool is_read_only() const;
    inline const StoreKeySet & get_write_set() const;
    inline void clear_write_set();
    inline bool has_conflict_with(const StatePtr &) const;
    inline bool write_set_contains(const StoreKey &) const;
    inline size_t get_pin_cnt() const;
    inline void increase_pin_cnt();
    inline void decrease_pin_cnt();
    void record_write(const StoreKey &);
    StatePtrOpt backtrack_latest_update(const StoreKey &);

    friend std::ostream & operator<<(std::ostream &, const State &);

  private:
    ClientIdSet ancestors;
    StatePtrSet parents;
    StatePtrVec children;
    ForkPointPtrSet fork_path;
    StoreKeySet write_set;
    size_t pin_cnt;

    inline void propagate_to_children(const ForkPointPtr &) const;
    inline void add(const ForkPointPtr &);
    inline void clone_fork_path(const StatePtr &);
    inline void clone_ancestors(const StatePtr &);
    inline void append_to(const StatePtr & state);
};

class Txn {
  public:
    const StatePtr state;
    const StatePtrVec * read_states;

    Txn(const StatePtr &, const StatePtrVec &);
    ~Txn();
    StoreValOpt get(const StoreKey &) const;
    StoreVal getForState(const StoreKey &, const StatePtr &) const;
    void put(const StoreKey &, const StoreVal &) const;
    inline bool is_read_states_valid() const;
    inline void rollback() const;

    friend std::ostream & operator<<(std::ostream &, const Txn &);

  private:
    const StorePtr store;
};

class StateDag {
  public:
    static const StateId ROOT_STATE_ID;
    static const ClientId ROOT_CLIENT_ID;

    static StateDagPtr instance();

    const StatePtr root;

    ~StateDag();
    size_t size() const;
    Txn beginTxn(const ClientId &);
    bool commitTxn(const Txn &);
    const ForkPointPtrSet & findForkPoints(const StatePtr &) const;
    StoreKeySetPtr findConflictWrites(const StatePtr &) const;
    StatePtrOpt backtrack_latest_update(const StoreKey &, const StatePtr &) const;
    size_t garbage_collect(const StateId &);

  private:
    StateId latest_state_id;
    StatePtrSet leaves;
    size_t cnt_state;

    SharedMutexPtr mtx_leaves;

    StateDag();
    inline StatePtr find_read_state(const ClientId &);
    inline StatePtr ripple_down(const StatePtr &, const StatePtr &) const;
    inline StateId get_state_id();
};

} // namespace tardis
} // namespace ustore

#endif /* TARDIS_CORE_H */
