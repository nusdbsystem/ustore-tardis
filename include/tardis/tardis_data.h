#ifndef TARDIS_DATA_H
#define TARDIS_DATA_H

#include <boost/tuple/tuple.hpp>

namespace ustore {
namespace tardis {

typedef uint64_t StoreKey;
typedef boost::tuple<int, double, std::string> StoreVal;
typedef uint64_t StoreVer;

typedef uint64_t StateId;
typedef uint32_t ClientId;

} // namespace tardis
} // namespace ustore

#endif /* TARDIS_DATA_H */
