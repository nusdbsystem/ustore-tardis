#ifndef TARDIS_APP_H
#define TARDIS_APP_H

#include <boost/random.hpp>
#include "tardis/tardis_core.h"

namespace ustore {
namespace tardis {

class Application;
class Workload;

typedef boost::mt19937 RandomGen;
typedef boost::random::uniform_int_distribution<StoreKey> StoreKeyUniformDistr;
typedef boost::random::variate_generator<RandomGen &, StoreKeyUniformDistr> StoreKeyUniformRandomGen;
typedef boost::random::uniform_int_distribution<uint32_t> UInt32UniformDistr;
typedef boost::random::variate_generator<RandomGen &, UInt32UniformDistr> UInt32UniformRandomGen;

class Application {
  public:
    static const StoreKey MIN_KEY;
    static const StoreKey MAX_KEY;
    static const size_t RO_TXN_NUM_OP;
    static const size_t RW_TXN_NUM_OP;

    Application();
    ~Application();
    size_t get_dag_size() const;
    size_t populate_store() const;
    bool read_only_task(const ClientId &);
    bool write_heavy_task(const ClientId &);
    bool read_heavy_task(const ClientId &);
    bool mix_task(const ClientId &);

  private:
    RandomGen rand_gen;
    StoreKeyUniformDistr key_uni_distr;
    StoreKeyUniformRandomGen rand_key;
    UInt32UniformDistr uint32_uni_distr;
    UInt32UniformRandomGen rand_100;

    const StateDagPtr dag;

    inline bool ro_txn(const ClientId &, const StoreKeyVec &) const;
    inline bool rw_txn(const ClientId &, const StoreKeyVec &) const;
};

class Workload {
  public:
    static void populate_store(const Application &);

    Workload(const Application &, const std::string &);
    virtual ~Workload();
    virtual void run(const size_t &, const size_t &);

  protected:
    Application app;
    const std::string workload_name;

    virtual bool task(const ClientId &) = 0;
};

class ReadOnlyWorkload: public Workload {
  public:
    ReadOnlyWorkload(const Application & app)
        : Workload(app, "read-only") {}

  protected:
    bool task(const ClientId & client_id) {
        return app.read_only_task(client_id);
    }
};

class WriteHeavyWorkload: public Workload {
  public:
    WriteHeavyWorkload(const Application & app)
        : Workload(app, "write-heavy") {}

  protected:
    bool task(const ClientId & client_id) {
        return app.write_heavy_task(client_id);
    }
};

class ReadHeavyWorkload: public tardis::Workload {
  public:
    ReadHeavyWorkload(const tardis::Application & app)
        : Workload(app, "read-heavy") {}

  protected:
    bool task(const tardis::ClientId & client_id) {
        return app.read_heavy_task(client_id);
    }
};

class MixWorkload: public Workload {
  public:
    MixWorkload(const Application & app)
        : Workload(app, "mix") {}

  protected:
    bool task(const ClientId & client_id) {
        return app.mix_task(client_id);
    }
};

} // namespace tardis
} // namespace ustore

#endif /* TARDIS_APP_H */
