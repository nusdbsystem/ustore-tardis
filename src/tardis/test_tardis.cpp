#include "boost_log.h"
#include "tardis/tardis_app.h"

#define LOG_LEVEL debug
#define LOG BOOST_LOG_TRIVIAL

static size_t DEFAULT_NUM_CLIENTS = 3;

namespace tardis = ustore::tardis;

template<class>
void run(tardis::Application &, const size_t &, const size_t & = DEFAULT_NUM_CLIENTS);

int
main(int argc, char *argv[]) {
    init_boost_log(boost::log::trivial::LOG_LEVEL);

    LOG(info) << "[ Start testing UStore-Tardis ]";

    tardis::Application app;
    tardis::Workload::populate_store(app);

    run<tardis::ReadOnlyWorkload>(app, 10);
    run<tardis::WriteHeavyWorkload>(app, 100);
    run<tardis::ReadHeavyWorkload>(app, 100);
    run<tardis::MixWorkload>(app, 100);

    LOG(info) << "[ End of testing UStore-Tardis ]";

    return 0;
}

template<class W>
void
run(tardis::Application & app,
    const size_t & num_tasks,
    const size_t & num_clients) {
    tardis::Workload * tw = new W(app);
    tw->run(num_tasks, num_clients);
    delete tw;
}
