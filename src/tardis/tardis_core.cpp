#include <algorithm>
#include <boost/foreach.hpp>
#include <boost/log/trivial.hpp>
#include <boost/none.hpp>
#include <sstream>
#include "tardis/tardis_core.h"

#define LOG BOOST_LOG_TRIVIAL

namespace ustore {
namespace tardis {

const StoreVer Store::INIT_STORE_VER = 0;
const StateId StateDag::ROOT_STATE_ID = 0;
const ClientId StateDag::ROOT_CLIENT_ID = 0;

StoreVal
Store::get_impl(const StoreKey & key,
                const StoreVer & store_ver) const {
    //TODO: read data from the multi-version key-value store
    return boost::make_tuple((int)key, 3.14, "something");
}

StoreVer
Store::put_impl(const StoreKey & key,
                const StoreVer & prev_store_ver,
                const StoreVal & value) const {
    //TODO: write data to the multi-version key-value store
    return 0;
}

void
Store::erase_impl(const StoreKey & key,
                  const StoreVer & store_ver) const {
    //TODO: delete data in the multi-version key-value store
}

StorePtr
Store::instance() {
    static StorePtr instance(new Store());

    return instance;
}

Store::Store()
    : ki2v(StoreKeyStateId2StoreVer()),
      mtx_ki2v(new SharedMutex()),
      k2i(StoreKey2StatePtrVecPtr()),
      mtx_k2i(new SharedMutex()),
      k2mtx_state_vec(StoreKey2SharedMutexPtr()),
      mtx_k2mtx_state_vec(new SharedMutex()) {}

Store::~Store() {}

StoreVal
Store::get(const StoreKey & key,
           const StatePtr & state) const {
    return get_impl(key, get_store_ver(key, state));
}

void
Store::put(const StoreKey & key,
           const StatePtr & state,
           const StoreVal & value) {
    add_key_version_mapping(key, state);

    //TODO: how should we properly define the "previous state"?
    ReadLock rlock_state_vec(*get_mutex_for_state_vec(key));
    const StatePtr prev_state = get_versions(key)->back();
    rlock_state_vec.unlock();

    const StoreVer prev_store_ver = get_store_ver(key, prev_state);
    const StoreVer new_store_ver = put_impl(key, prev_store_ver, value);

    add_store_ver_mapping(key, state, new_store_ver);
}

void
Store::erase(const StoreKey & key,
             const StatePtr & state) {
    delete_key_version_mapping(key, state);
    erase_impl(key, get_store_ver(key, state));
    delete_store_ver_mapping(key, state);
}

StatePtrVecPtr
Store::get_versions(const StoreKey & key) const {
    ReadLock rlock_k2i(*mtx_k2i);

    return k2i.find(key)->second;
}

SharedMutexPtr
Store::get_mutex_for_state_vec(const StoreKey & key) const {
    ReadLock rlock_k2mtx_state_vec(*mtx_k2mtx_state_vec);

    return k2mtx_state_vec.find(key)->second;
}

void
Store::add_key_version_mapping(const StoreKey & key,
                               const StatePtr & state) {
    if (k2i.count(key) == 0) { // initialize the entry
        WriteLockGuard wguard_k2i(*mtx_k2i);

        if (k2i.count(key) == 0) {
            LOG(trace) << "creating key-version entry for key " << key;

            const StatePtrVecPtr state_vec(new StatePtrVec());
            k2i.insert(std::make_pair(key, state_vec));

            SharedMutexPtr mtx_state_vec(new SharedMutex());
            WriteLockGuard wguard_k2mtx_state_vec(*mtx_k2mtx_state_vec);
            k2mtx_state_vec.insert(std::make_pair(key, mtx_state_vec));
        }
    }

    WriteLockGuard wguard_state_vec(*get_mutex_for_state_vec(key));
    get_versions(key)->push_back(state);
}

void
Store::delete_key_version_mapping(const StoreKey & key,
                                  const StatePtr & state) {
    if (k2i.count(key) == 0) {
        LOG(trace) << "trivial deletion due to inexistence of key " << key;

    } else {
        WriteLockGuard wguard_state_vec(*get_mutex_for_state_vec(key));
        StatePtrVecPtr state_vec = get_versions(key);
        state_vec->erase(std::find(state_vec->begin(), state_vec->end(), state));
    }
}

StoreVer
Store::get_store_ver(const StoreKey & key,
                     const StatePtr & state) const {
    const PairStoreKeyStateId ki(key, state->id);
    ReadLock rlock_ki2v(*mtx_ki2v);

    StoreKeyStateId2StoreVer::const_iterator got = ki2v.find(ki);

    if (got == ki2v.end()) {
        return INIT_STORE_VER;

    } else {
        return got->second;
    }
}

void
Store::add_store_ver_mapping(const StoreKey & key,
                             const StatePtr & state,
                             const StoreVer & store_ver) {
    const PairStoreKeyStateId ki(key, state->id);

    WriteLockGuard wguard_ki2v(*mtx_ki2v);
    ki2v.insert(std::make_pair(ki, store_ver));
}

void
Store::delete_store_ver_mapping(const StoreKey & key,
                                const StatePtr & state) {
    const PairStoreKeyStateId ki(key, state->id);

    WriteLockGuard wguard_ki2v(*mtx_ki2v);
    ki2v.erase(ki);
}

ForkPoint::ForkPoint(const StatePtr & state,
                     const int & branch_id)
    : state(state),
      branch_id(branch_id) {}

ForkPoint::~ForkPoint() {}

bool
ForkPoint::operator==(const ForkPointPtr & that) const {
    return this->state->id == that->state->id &&
           this->branch_id == that->branch_id;
}

bool
ForkPoint::operator<(const ForkPointPtr & that) const {
    if (this->state->id == that->state->id) {
        assert(this->branch_id != that->branch_id);
        return this->branch_id < that->branch_id;

    } else {
        return this->state->id < that->state->id;
    }
}

inline std::ostream &
operator<<(std::ostream & os, const ForkPoint & p) {
    return os << "S-" << p.state->id << "-" << p.branch_id;
}

State::State(const StateId & id,
             const ClientId & client_id)
    : id(id),
      client_id(client_id),
      is_valid(false),
      ancestors(ClientIdSet()),
      parents(StatePtrSet()),
      children(StatePtrVec()),
      mtx_children(new SharedMutex()),
      fork_path(ForkPointPtrSet()),
      mtx_fork_path(new SharedMutex()),
      write_set(StoreKeySet()),
      pin_cnt(0),
      mtx_pin_cnt(new SharedMutex()) {
    ancestors.insert(client_id);
}

State::~State() {}

bool
State::operator==(const StatePtr & that) const {
    return this->id == that->id;
}

bool
State::operator<(const StatePtr & that) const {
    return this->id < that->id;
}

bool
State::has_ancestor_of(const ClientId & client_id) const {
    return ancestors.find(client_id) != ancestors.end();
}

const StatePtrSet &
State::get_parents() const {
    return parents;
}

const StatePtrVec &
State::get_children() const {
    return children;
}

void
State::add_child(const StatePtr & child) {
    WriteLockGuard wguard_children(*mtx_children);

    children.push_back(child);

    const size_t num_branches = children.size();

    if (num_branches < 2) {
        // do nothing

    } else if (num_branches == 2) {
        LOG(debug) << "branching at S-" << id;

        for (int b = 0; b < num_branches; ++b) {
            const ForkPointPtr p(new ForkPoint(shared_from_this(), b));
            children[b]->add_fork_point(p);
        }

    } else { // num_branches > 2
        LOG(debug) << "creating a new branch at S-" << id;

        const int b = num_branches - 1;
        const ForkPointPtr p(new ForkPoint(shared_from_this(), b));
        child->add_fork_point(p);
    }
}

void
State::add_fork_point(const ForkPointPtr & fork_point) {
    add(fork_point);
    propagate_to_children(fork_point);
}

void
State::add(const ForkPointPtr & p) {
    WriteLock wlock_fork_path(*mtx_fork_path);
    fork_path.insert(p);
}

void
State::propagate_to_children(const ForkPointPtr & p) const {
    ReadLock rlock_children(*mtx_children);

    BOOST_FOREACH(StatePtr child, children) {
        LOG(debug) << "propagating fork point " << *p << " to S-"
                   << child->id;

        child->add_fork_point(p);
    }
}

const ForkPointPtrSet &
State::get_fork_path() const {
    return fork_path;
}

void
State::commit_after(const StatePtr & commit_state) {
    assert(commit_state.get() != NULL);

    clone_fork_path(commit_state);
    clone_ancestors(commit_state);
    append_to(commit_state);

    is_valid = true;
    // Note: upon attached to the DAG, the state becomes valid.
}

void
State::clone_fork_path(const StatePtr & that) {
    if (this->id == that->id) {
        LOG(trace) << "trivial to clone fork path of itself";

    } else {
        ReadLock rlock_remote_fork_path(*(that->mtx_fork_path));
        WriteLockGuard guard_fork_path(*mtx_fork_path);

        BOOST_FOREACH(ForkPointPtr p, that->fork_path) {
            this->fork_path.insert(p);
        }
    }
}

void
State::clone_ancestors(const StatePtr & that) {
    if (this->id == that->id) {
        LOG(trace) << "trivial to clone ancestors of itself";

    } else {
        BOOST_FOREACH(ClientId cid, that->ancestors) {
            this->ancestors.insert(cid);
        }
    }
}

void
State::append_to(const StatePtr & state) {
    assert(parents.empty());
    assert(this->id != state->id);

    LOG(debug) << "appending S-" << this->id << " to S-" << state->id;

    parents.insert(state);
    state->add_child(shared_from_this());
}

bool
State::is_compatible_with(const StatePtr & that) const {
    if (that->is_valid) {

        bool is_compatible = true;

        ReadLock rlock_this_fork_path(*(this->mtx_fork_path));
        ReadLock rlock_that_fork_path(*(that->mtx_fork_path));

        BOOST_FOREACH(ForkPointPtr p, that->get_fork_path()) {
            if (fork_path.find(p) == fork_path.end()) {
                is_compatible = false;
                break;
            }
        }

        return is_compatible;

    } else {
        LOG(trace) << "an invalid state is always incompatible";

        return false;
    }
}

bool
State::is_leaf() const {
    ReadLock rlock_children(*mtx_children);

    return children.empty();
}

void
State::record_write(const StoreKey & key) {
    write_set.insert(key);
}

bool
State::is_read_only() const {
    return write_set.empty();
}

const StoreKeySet &
State::get_write_set() const {
    return write_set;
}

void
State::clear_write_set() {
    write_set.clear();
}

bool
State::has_conflict_with(const StatePtr & that) const {
    if (
        that->id == this->id ||
        (parents.find(that) != parents.end()) ||
        std::find(children.begin(), children.end(), that) != children.end()
    ) {
        LOG(trace) << "trivially having no conflict";
        return false;
    }

    bool has_conflict = false;

    BOOST_FOREACH(StoreKey k, write_set) {
        if (that->write_set_contains(k)) {
            has_conflict = true;
            break;
        }
    }

    return has_conflict;
}

bool
State::write_set_contains(const StoreKey & key) const {
    return write_set.find(key) != write_set.end();
}

size_t
State::get_pin_cnt() const {
    return pin_cnt;
}

void
State::increase_pin_cnt() {
    WriteLockGuard wguard_pin_cnt(*mtx_pin_cnt);

    ++pin_cnt;
}

void
State::decrease_pin_cnt() {
    WriteLockGuard wguard_pin_cnt(*mtx_pin_cnt);

    assert(pin_cnt > 0);
    --pin_cnt;
}

StatePtrOpt
State::backtrack_latest_update(const StoreKey & key) {
    StatePtrQue queue;
    StatePtrSet accessed;

    accessed.insert(shared_from_this());
    queue.push(shared_from_this());

    while (!queue.empty()) {
        StatePtr state = queue.front();
        queue.pop();

        if (state->write_set_contains(key)) {
            return state;

        } else {
            BOOST_FOREACH(StatePtr parent, state->get_parents()) {
                if (accessed.find(parent) == accessed.end()) {
                    accessed.insert(state);
                    queue.push(parent);
                }
            }
        }
    }

    return boost::none;
}

inline std::ostream &
operator<<(std::ostream & os, const State & state) {
    ReadLock rlock_pin_cnt(*(state.mtx_pin_cnt));

    os << "S-" << state.id << "(valid: " << state.is_valid
       << ", pin: " << state.pin_cnt << ", fork: {";

    rlock_pin_cnt.unlock();

    ReadLock rlock_fork_path(*(state.mtx_fork_path));

    if (!state.fork_path.empty()) {
        ForkPointPtrSet::const_iterator it = state.fork_path.begin();
        os << *it;

        for (++it; it != state.fork_path.end(); ++it) {
            os << ", " << *it;
        }
    }

    rlock_fork_path.unlock();

    os << "}, ancestors: {";

    if (!state.ancestors.empty()) {
        ClientIdSet::const_iterator it = state.ancestors.begin();
        os << "C-" << *it;

        for (++it; it != state.ancestors.end(); ++it) {
            os << ", C-" << *it;
        }
    }

    os << "})";

    return os;
}

Txn::Txn(const StatePtr & txn_state,
         const StatePtrVec & read_states)
    : state(txn_state),
      read_states(new StatePtrVec(read_states)),
      store(Store::instance()) {
    assert(this->read_states->size() > 0);

    BOOST_FOREACH(StatePtr s, *(this->read_states)) {
        s->increase_pin_cnt();
    }
}

Txn::~Txn() {
    BOOST_FOREACH(StatePtr s, *read_states) {
        s->decrease_pin_cnt();
    }

    delete read_states;
}

StoreValOpt
Txn::get(const StoreKey & key) const {
    assert(read_states->size() == 1);
    // Note: it is irrational to read from multiple read states.

    StatePtr read_state = read_states->front();
    StatePtr ver_state;

    ReadLock rlock_state_vec(*(store->get_mutex_for_state_vec(key)));
    const StatePtrVecPtr state_vec = store->get_versions(key);

    BOOST_REVERSE_FOREACH(StatePtr s, *(state_vec)) {
        if (read_state->is_compatible_with(s)) {
            ver_state = s;
            break;
        }
    }

    rlock_state_vec.unlock();

    if (ver_state.get() != NULL) {
        const StoreValOpt value_opt = getForState(key, ver_state);

        LOG(trace) << "got r(" << key << ", S-" << ver_state->id << ") -> "
                   << value_opt->get<0>() << ", "
                   << value_opt->get<1>() << ", "
                   << value_opt->get<2>();

        return value_opt;

    } else {
        LOG(debug) << "no r(" << key << ") is available to T-"
                   << state->id;

        return boost::none;
    }
}

StoreVal
Txn::getForState(const StoreKey & key,
                 const StatePtr & ver_state) const {
    return store->get(key, ver_state);
}

void
Txn::put(const StoreKey & key,
         const StoreVal & value) const {
    LOG(trace) << "put r(" << key << ", S-" << state->id << ") -> "
               << value.get<0>() << ", "
               << value.get<1>() << ", "
               << value.get<2>();

    store->put(key, state, value);
    state->record_write(key);
}

bool
Txn::is_read_states_valid() const {
    bool is_valid = true;

    BOOST_FOREACH(StatePtr s, *read_states) {
        if (!s->is_valid) {
            is_valid = false;
            break;
        }
    }

    return is_valid;
}

void
Txn::rollback() const {
    LOG(debug) << "T-" << state->id << " is rolling back";

    BOOST_FOREACH(StoreKey k, state->get_write_set()) {
        LOG(debug) << "delete r(" << k << ", S-" << state->id << ")";

        store->erase(k, state);
    }

    state->clear_write_set();
}

inline std::ostream &
operator<<(std::ostream& os, const Txn & txn) {
    os << "T-" << txn.state->id << "(client: " << txn.state->client_id
       << ", read: {";

    if (!txn.read_states->empty()) {
        StatePtrVec::const_iterator it = txn.read_states->begin();
        os << "S-" << (*it)->id;

        for (++it; it != txn.read_states->end(); ++it) {
            os << ", S-" << (*it)->id;
        }
    }

    os << "})";

    return os;
}

StateDag::StateDag()
    : root(new State(get_state_id(), ROOT_CLIENT_ID)),
      leaves(StatePtrSet()),
      mtx_leaves(new SharedMutex()),
      cnt_state(1) {
    leaves.insert(root);
    root->is_valid = true;

    LOG(debug) << "S: state, C: client, T: transaction, r: record";
}

StateDag::~StateDag() {}

StateDagPtr
StateDag::instance() {
    static StateDagPtr instance(new StateDag());

    return instance;
}

size_t
StateDag::size() const {
    return cnt_state;
}

Txn
StateDag::beginTxn(const ClientId & client_id) {
    StatePtrVec read_states;
    read_states.push_back(find_read_state(client_id));
    const StatePtr txn_state(new State(get_state_id(), client_id));
    const Txn txn(txn_state, read_states);

    LOG(debug) << txn << ", " << *(txn.read_states->front());

    return txn;
}

StatePtr
StateDag::find_read_state(const ClientId & client_id) {
    StatePtr read_state;

    ReadLock rlock_leaves(*mtx_leaves);

    BOOST_FOREACH(StatePtr s, leaves) {
        if (s->has_ancestor_of(client_id)) {
            read_state = s;
            break;
        }
    }

    rlock_leaves.unlock();

    if (read_state.get() == NULL) {
        LOG(trace) << "C-" << client_id
                   << " is new, and thus it reads from any leaf";

        read_state = *(leaves.begin());
    }

    return read_state;
}

bool
StateDag::commitTxn(const Txn & txn) {
    static Mutex mtx_cnt_state;

    if (txn.is_read_states_valid()) {
        if (txn.state->is_read_only()) {
            LOG(trace) << "T-" << txn.state->id << " is read-only and thus "
                       << "it is unnecessary to record its state into the DAG";

            return true;

        } else {
            const StatePtr read_state = txn.read_states->front();
            const StatePtr commit_state = ripple_down(read_state, txn.state);
            txn.state->commit_after(commit_state);

            WriteLock wlock_leaves(*mtx_leaves);

            BOOST_FOREACH(StatePtr s, txn.state->get_parents()) {
                leaves.erase(s);
            }

            leaves.insert(txn.state);

            wlock_leaves.unlock();

            LockGuard guard_cnt_state(mtx_cnt_state);

            ++cnt_state;

            return true;
            // Note: as long as the read states are valid, transaction will
            //       always commit, i.e., at least appended to a read state.
        }

    } else {
        LOG(debug) << "T-" << txn.state->id << " aborts "
                   << "due to invalid read state(s)";

        txn.rollback();

        return false;
    }
}

StatePtr
StateDag::ripple_down(const StatePtr & begin_state,
                      const StatePtr & sign_state) const {
    assert(begin_state->is_valid);

    StatePtr end_state = begin_state;
    bool is_latest_valid = false;

    while (!is_latest_valid) {
        if (end_state->is_leaf()) {
            is_latest_valid = true;

        } else {
            bool is_ripple_down = false;

            ReadLock rlock_remote_children(*(end_state->mtx_children));

            BOOST_FOREACH(StatePtr child, end_state->get_children()) {
                if (child->is_valid && !child->has_conflict_with(sign_state)) {
                    LOG(debug) << "rippling down: S-" << end_state->id
                               << " -> S-" << child->id;

                    end_state = child;
                    is_ripple_down = true;
                    break;
                }
            }

            rlock_remote_children.unlock();

            if (!is_ripple_down) {
                is_latest_valid = true;
            }
        }
    }

    return end_state;
}

const ForkPointPtrSet &
StateDag::findForkPoints(const StatePtr & state) const {
    return state->get_fork_path();
}

StoreKeySetPtr
StateDag::findConflictWrites(const StatePtr & state) const {
    StoreKeySet keys;
    StoreKeySetPtr conflict_keys(new StoreKeySet());

    ReadLock rlock_children(*(state->mtx_children));

    BOOST_FOREACH(StatePtr child, state->get_children()) {
        BOOST_FOREACH(StoreKey k, child->get_write_set()) {
            if (keys.find(k) == keys.end()) {
                keys.insert(k);

            } else {
                conflict_keys->insert(k);
            }
        }
    }

    return conflict_keys;
}

StatePtrOpt
StateDag::backtrack_latest_update(const StoreKey & key,
                                  const StatePtr & from_state) const {
    return from_state->backtrack_latest_update(key);
}

size_t
garbage_collect(const StateId & ceil_state_id) {
    size_t cnt_gc = 0;

    //TODO

    return cnt_gc;
}

StateId
StateDag::get_state_id() {
    static Mutex mtx_id;
    static StateId id = ROOT_STATE_ID - 1;

    LockGuard guard_id(mtx_id);

    latest_state_id = ++id;

    return latest_state_id;
}

} // namespace tardis
} // namespace ustore
