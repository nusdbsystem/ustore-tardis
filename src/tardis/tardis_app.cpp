#include <boost/foreach.hpp>
#include <boost/log/trivial.hpp>
#include "tardis/tardis_app.h"

#define LOG BOOST_LOG_TRIVIAL

namespace ustore {
namespace tardis {

const StoreKey Application::MIN_KEY = 0x0;
const StoreKey Application::MAX_KEY = 0xff;
const size_t Application::RO_TXN_NUM_OP = 6;
const size_t Application::RW_TXN_NUM_OP = 3;

Application::Application()
    : dag(StateDag::instance()),
      rand_gen(RandomGen(std::time(0))),
      key_uni_distr(StoreKeyUniformDistr(MIN_KEY, MAX_KEY)),
      rand_key(StoreKeyUniformRandomGen(rand_gen, key_uni_distr)),
      uint32_uni_distr(UInt32UniformDistr(0, 99)),
      rand_100(UInt32UniformRandomGen(rand_gen, uint32_uni_distr)) {}

Application::~Application() {}

size_t
Application::get_dag_size() const {
    return dag->size();
}

size_t
Application::populate_store() const {
    const StorePtr store(Store::instance());
    const double vDouble = 3.1415926;
    const std::string vString = "dummy";

    for (StoreKey k = MIN_KEY; k <= MAX_KEY; ++k) {
        const int vInt = k;
        store->put(k, dag->root, boost::make_tuple(vInt, vDouble, vString));
    }

    return MAX_KEY - MIN_KEY + 1;
}

bool
Application::ro_txn(const ClientId & client_id,
                    const StoreKeyVec & op_key_set) const {
    const Txn txn = dag->beginTxn(client_id);

    volatile int sum = 0;

    BOOST_FOREACH(StoreKey key, op_key_set) {
        StoreValOpt value_opt = txn.get(key);

        if (value_opt) {
            sum += value_opt->get<0>();
        }
    }

    LOG(trace) << "sum=" << sum;

    return dag->commitTxn(txn);
}

bool
Application::rw_txn(const ClientId & client_id,
                    const StoreKeyVec & op_key_set) const {
    const Txn txn = dag->beginTxn(client_id);

    BOOST_FOREACH(StoreKey key, op_key_set) {
        StoreValOpt value_opt = txn.get(key);

        if (value_opt) {
            const int vInt = value_opt->get<0>();
            const double vDouble = value_opt->get<1>();
            const std::string vString = value_opt->get<2>();

            txn.put(key, boost::make_tuple(vInt, vDouble, vString));
        }
    }

    return dag->commitTxn(txn);
}

bool
Application::read_only_task(const ClientId & client_id) {
    StoreKeyVec op_key_set;

    for (int i = 0; i < RO_TXN_NUM_OP; ++i) {
        op_key_set.push_back(rand_key());
    }

    return ro_txn(client_id, op_key_set);
}

bool
Application::write_heavy_task(const ClientId & client_id) {
    StoreKeyVec op_key_set;

    for (int i = 0; i < RW_TXN_NUM_OP; ++i) {
        op_key_set.push_back(rand_key());
    }

    return rw_txn(client_id, op_key_set);
}

bool
Application::read_heavy_task(const ClientId & client_id) {
    if (rand_100() < 75) {
        return read_only_task(client_id);

    } else {
        return write_heavy_task(client_id);
    }
}

bool
Application::mix_task(const ClientId & client_id) {
    if (rand_100() < 25) {
        return read_only_task(client_id);

    } else {
        return write_heavy_task(client_id);
    }
}

void
Workload::populate_store(const Application & app) {
    LOG(info) << "Populating data store...";

    const size_t num_tuples = app.populate_store();

    LOG(info) << "Done: " << num_tuples << " tuples have been inserted. "
              << "DAG size: " << app.get_dag_size();
}

Workload::Workload(const tardis::Application & app,
                   const std::string & name)
    : app(app),
      workload_name(name) {}

Workload::~Workload() {}

void
Workload::run(const size_t & num_tasks,
              const size_t & num_clients) {
    LOG(info) << "Running " << workload_name << " tasks with "
              << num_clients << " clients... " ;

    uint32_t cnt_commit = 0;

    for (int i = 0; i < num_tasks; ++i) {
        const bool is_commit = task(i % num_clients + 1);

        if (is_commit) ++cnt_commit;
    }

    LOG(info) << "Done: " << cnt_commit << " out of " << num_tasks
              <<  " " << workload_name << " tasks have been committed. "
              << "DAG size: " << app.get_dag_size();
}

} // namespace tardis
} // namespace ustore
