#include <iomanip>
#include "boost_log.h"

namespace logging = boost::log;

void init_boost_log(const logging::trivial::severity_level & lvl) {
    logging::add_console_log
    (
        std::clog,
        logging::keywords::format =
            (
                logging::expressions::stream
                << std::setw(5) << std::setfill(' ')
                << logging::trivial::severity << " > "
                << logging::expressions::smessage
            )
    );

    logging::core::get()->set_filter
    (
        logging::trivial::severity >= lvl
    );
}
